﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModuleTwoAssignment
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    if ((j % 2 == 0 && i % 2 == 0) || (j % 2 != 0 && i % 2 != 0))
                    //if j and i are both odd or both even. it will return X.
                    {
                        Console.Write("X");
                    }
                    else Console.Write("O");
                }
                Console.Write("\n");
            }
           
        }
    }
}
