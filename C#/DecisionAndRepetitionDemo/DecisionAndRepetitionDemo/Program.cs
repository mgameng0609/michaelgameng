﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DecisionAndRepetitionDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            #region if, if else, if else if, switch
            int condition2;
            int switchCondition;

            Console.WriteLine("Enter number greater than 10: ");
            string number = Console.ReadLine();
            int condition1 = Convert.ToInt32(number);
            if (condition1 > 10)
            {
                Console.WriteLine("Number entered is greater than 10");
            }
            else
            {
                Console.WriteLine("Number not greater than 10");
            }
            #endregion
            #region for, foreach, while, dowhile
            int whileCounter = 0;
            int doCounter = 0;

            for (int forCounter = 0; forCounter < 10; forCounter++)
            {
                Console.WriteLine(forCounter);
            }
            #endregion
        }
    }
}
