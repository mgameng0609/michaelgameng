﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModuleOneAssignment
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter Student's first name: ");
            string firstName = Console.ReadLine();
            Console.WriteLine("Enter Student's last name: ");
            string lastName = Console.ReadLine();

            Console.WriteLine("Enter month of birth (January, February, etc.): ");
            string birthMonth = Console.ReadLine();
            Console.WriteLine("Enter date of birth: ");
            string birthDay = Console.ReadLine();
            Console.WriteLine("Enter year of birth: ");
            string birthYear = Console.ReadLine();
            string birthDate = String.Format("{0} {1}, {2}", birthMonth, birthDay, birthYear);
         
            Console.WriteLine("Enter address line 1: ");
            string addressLine1 = Console.ReadLine();
            Console.WriteLine("Enter address line 2: ");
            string addressLine2 = Console.ReadLine();
            Console.WriteLine("Enter city: ");
            string city = Console.ReadLine();
            Console.WriteLine("Enter 2 letter abbreviation of state: ");
            string stateProvince = Console.ReadLine();
            Console.WriteLine("Enter zip code: ");
            string zipCode = Console.ReadLine();
            Console.WriteLine("Enter abbreviation of country: ");
            string country = Console.ReadLine();
            
            Console.WriteLine("{0} {1}", firstName, lastName);
            Console.WriteLine(birthDate);
            Console.WriteLine("{0}{1}, {2}, {3} {4} {5}", addressLine1, addressLine2, city, stateProvince, zipCode, country);
            
            
           
        }
    }
}
