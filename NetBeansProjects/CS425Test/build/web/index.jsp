<%-- 
    Document   : index
    Created on : Mar 14, 2016, 1:43:38 PM
    Author     : MichaelGameng
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="style.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Illinois Institute of Technology</title>
    </head>
    <body>
        <h1>Welcome to IIT Student/Faculty Network Site!</h1>
        <table border="0">
            <thead>
                <tr>
                    <th>This website will allow you to stay updated with IIT.</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Register or sign in below.</td>
                </tr>
                <tr>
                    <td><form action="login.jsp">
                            <input type="submit" value="Log In" name="Log In" />
                        </form></td>
                        
                </tr>
                <tr><td><form action="register.jsp">
                                <input type="submit" value="Register" name="Register" />
                            </form></td></tr>
            </tbody>
        </table>

    </body>
</html>
