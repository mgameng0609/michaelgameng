<%-- 
    Document   : register
    Created on : Mar 14, 2016, 2:10:00 PM
    Author     : MichaelGameng
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>


<html>
    <head>
        <link rel="stylesheet" type="text/css" href="style.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registration Page</title>
    </head>
    <body>
        <h1>Hello, please register below:</h1>
        <table border="0">
            <thead>
                <tr>
                    <th>Fill in the details</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><form action="registerServlet" method=post>
                        <div id="registrationBox">
                            <p><strong>username:</strong>
                                <input type="username" size="20" name="username"></p>
                            <p><strong>password:</strong>
                                <input type="password" size="20" name="password"></p>
                            <p><strong>confirm password:</strong>
                                <input type="confirmPassword" size="20" name="confirmPassword"></p>
                            <p><strong>email:</strong>
                                <input type="email" size="20" name="email"</p>
                            <p><input type="submit" value="submit"></p>
                        </div>
                    </form></td>
                </tr>
            </tbody>
        </table>
    </body>
</html>