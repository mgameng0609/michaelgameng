<%-- 
    Document   : error
    Created on : Mar 14, 2016, 1:54:51 PM
    Author     : MichaelGameng
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div id="loginBox">

    <p class="error">Invalid username or password.</p>

    <p>Return to <strong><a href="login.jsp">admin login</a></strong>.</p>

</div>
