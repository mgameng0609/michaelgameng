<%-- 
    Document   : login
    Created on : Mar 14, 2016, 1:54:35 PM
    Author     : MichaelGameng
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="style.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Log In Page</title>
    </head>
    <body>
        <h1>Hello, please log in below:</h1>
        <table border="0">
            <thead>
                <tr>
                    <th>Fill your username and password</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><form action="loginServlet" method=post>
    <div id="loginBox">
        <p><strong>username:</strong>
            <input type="username" size="20" name="username"></p>

        <p><strong>password:</strong>
            <input type="password" size="20" name="password"></p>

        <p><input type="submit" value="submit"></p>
    </div>
</form></td></tr>
            </tbody>
        </table>
    </body>
</html>
