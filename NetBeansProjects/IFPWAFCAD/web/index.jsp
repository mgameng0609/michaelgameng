<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%-- 
    Document   : index
    Created on : Mar 14, 2016, 12:18:57 PM
    Author     : MichaelGameng
--%>
<sql:query var="subject" dataSource="jdbc/IFPWAFCAD">
    SELECT subject_id, name FROM Subject
</sql:query>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="style.css">
        <title>First WebApp Homepage</title>
    </head>
    <body>
        <h1>Welcome to my first webapp</h1>
        <table border="0">
            <thead>
                <tr>
                    <th>This is just for my own learning</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                    Select below:</td>
                </tr>
                <tr>
                    <td><form action="response.jsp">
                            <strong>Select a name:</strong>
                            <select name="subject_id">
                                <c:forEach var="row" items="${subject.rows}">
                                    <option value="${row.subject_id}">${row.name}</option>
                                </c:forEach>
                            </select>
                            <input type="submit" value="submit" name="submit" />
                        </form></td>
                </tr>
            </tbody>
        </table>

    </body>
</html>
