import java.util.Scanner;
import java.text.DecimalFormat;

public class Lab5Number45
{
	public static void main(String []args)
	{
	int count = 0;
	int total = 0;
	final int SENTINEL = -1;
	double number;
	
	Scanner scan = new Scanner(System.in);
	
	System.out.println("To calculate average, ");
	System.out.println("enter each number." );
	System.out.println("You can type in a decimal.");
	System.out.println("When you are finished, enter a -1" );
	
	System.out.print("Enter the first number > ");
	number = scan.nextDouble();
	
	while(number != SENTINEL)
	{
		total += number;	
		count++;
		
		System.out.print("Enter the next number > ");
		number = scan.nextDouble();
	}
	
	if (count != 0)
	{
		DecimalFormat twoDecimalPlace = new DecimalFormat( "##.00");
		System.out.println( "\nThe average is " + twoDecimalPlace.format((double)(total)/count));
	}
		else
			System.out.println("\nNo numbers were entered");
	
	}
}