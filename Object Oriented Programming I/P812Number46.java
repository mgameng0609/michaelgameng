import java.util.Scanner;
import java.io.File;
import java.io.IOException;

public class P812Number46 {
	public static void main(String [] args) throws IOException
	{
		String n;
		int i = 0;
		
		File inputFile = new File("input3.txt");
		Scanner scan = new Scanner(inputFile);
		
		while(scan.hasNext())
		{
			n = scan.next();
			i++;
		}
		System.out.println("Number of lines: " + i);
	}

}
