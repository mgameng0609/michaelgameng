import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.FileNotFoundException;
import java.io.File;
import java.util.Scanner;
import java.io.IOException;

public class P812Number43
{
	public static void main(String [] args) throws Exception
	{
		File inputFile = new File("input1.txt");
		Scanner scan = new Scanner(inputFile);
		int n = 0;
		String m;
		while(scan.hasNext())
		{
			m = scan.next();
			n++;
			try
			{
				FileOutputStream fos = new FileOutputStream("backup.txt",false);
				PrintWriter pw = new PrintWriter(fos);
				pw.println(n + m);
				pw.close();
			}
			catch(FileNotFoundException fnfe)
			{
				System.out.println("Unable to find backup.txt");
			}
		}
	}
}
