import java.util.Scanner;
import java.io.File;
import java.io.IOException;

public class P812Number47 {
	public static void main(String [] args) throws IOException
	{
		double n;
		double m = 0;
		int i = 0;
		
		File inputFile = new File("input2.txt");
		Scanner scan = new Scanner(inputFile);
		
		while(scan.hasNextDouble())
		{
			n = scan.nextDouble();
			m += n;
			i++;
		}
		n = m / i;
		System.out.println("Average is "+ n);
	}

}
