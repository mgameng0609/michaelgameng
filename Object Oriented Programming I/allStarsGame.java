import java.util.Scanner;

public class allStarsGame
{
	public static void main(String [] args)
	{
		Scanner scan = new Scanner(System.in);
		
		System.out.print("Enter number of hits: ");
		double hits = scan.nextDouble();
		System.out.println("Number of hits = " + hits);
		
		System.out.print("Enter number of at-bats: ");
		double atBats = scan.nextDouble();
		System.out.println("Number of at-bats = " + atBats);
		
		double hittingPercentage = (hits / atBats);
		System.out.println("Your hitting percentage is " + hittingPercentage);
		
		if (hittingPercentage >= .300)
			System.out.println("You are eligible for the All Stars Game");
		else
			System.out.println("You are not eligible for the All Stars Game");
			
	}
}
		
	