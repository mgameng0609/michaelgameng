import java.util.Scanner;

public class Lab5Number49
{
	public static void main(String [] args)
	{
		Scanner scan = new Scanner(System.in);
		
		int n;
		int count = 0;
		
		System.out.print("Enter how many times you want to say 'Hello World' > ");
		if ( !scan.hasNextInt() )
			{
				System.out.print("YOU NEED TO PUT A NUMBER");
			}
		else
		{
		n = scan.nextInt();
		
		
		while (count < n)
		{
			System.out.println("Hello World");
			count++;
			
		}
		}
	}
}