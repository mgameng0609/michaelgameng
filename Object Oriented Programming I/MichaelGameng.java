//Michael Gameng
//A20303150
//CS115-02
import java.io.*;
import java.util.Scanner;

class MichaelGameng
{
	private String firstName;
	private String lastName;
	private int yearsInCompany;
	private double salary;
	private char status;
	private String section;
	private String divisionTitle;
	private int numberOfEmployeesInDivision;
	public static void main(String [] args) throws IOException
	{
		Scanner scan = new Scanner(System.in);
		
		File ifile = new File("empcs115.txt");
		Scanner scan2 = new Scanner(ifile);
		
		while(scan2.hasNext())
		{
		String input = scan2.next();
		String input1 = input.toLowerCase();
		}
		
		String data;
		//prompts the user if they want to call the menu or final stats
		System.out.print("Enter 'M' or 'm' to continue to the menu" +
						"\nEnter 'Q' or 'q' to display the number of times each menu option is entered" +
						"\nEnter your choice: ");
						
		String choice1 = scan.next();
		char choice = choice1.charAt(0);
		
		switch(choice)
		{
			case 'M': case 'm':
				data = menu(choice);
				break;
			case 'Q': case 'q':
				
				break;
			default:
				System.out.print("Try again: ");
			
				break;
		}
		
	}
	
	public static String menu(char choice) throws IOException
		{
		Scanner scan = new Scanner(System.in);
		System.out.println("Press 'L' or 'l' for all of the employee data" +
						"\nPress 'E' or 'e' for a particular employee" +
						"\nPress 'D' or 'd' for division information" +
						"\nPress 'S' or 's' for salary information" +
						"\nPress 'R' or 'r' for retirement information" +
						"\nPress 'Q' or 'q' to quit" +
						"\nEnter your choice: ");
		String input = scan.next();
		choice = input.charAt(0);
		File ifile = new File("empcs115.txt");
		Scanner scan2 = new Scanner(ifile);
		switch(choice)
		{
			case 'L': case 'l':
				System.out.println("Displaying all employee information: ");
				while(scan2.hasNext())	
					System.out.println(scan2.next());
				break;
			case 'E': case 'e':
				System.out.print("Employee's first and last name: ");
				String particularEmployee = scan.next();
				String employee = particularEmployee.toLowerCase();
				break;
			case 'D': case 'd':
				System.out.println("Division: ");
				break;
			default:
				System.out.print("Try again: ");
		}
		return(input);
		}
		
		
	
	
}