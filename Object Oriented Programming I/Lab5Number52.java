import java.util.Scanner;

public class Lab5Number52
{
	public static void main(String []args)
	{
		Scanner scan = new Scanner(System.in);
		int count = 0;
		final int total = 10;
		int min;
		int n;
		
		System.out.println("You will be asked to enter 10 integer values.");
		System.out.print("Enter first number > ");
		if ( !scan.hasNextInt() )
			{
				System.out.println("YOU NEED TO PUT A NUMBER");
			}
		else
			{
			min = scan.nextInt();
			while (scan.hasNextInt())
				{
					n = scan.nextInt();
					if (n < min)
					min = n;
				}
					System.out.println("The minimum is" + min);
			}
		
	}
}