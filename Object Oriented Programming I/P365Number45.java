import java.util.Scanner;
import java.io.*;

public class P365Number45 {
	public static void main(String [] args) throws IOException
	{
		double n;
		double m = 0;
		int i = 0;
		
		File inputFile = new File("input.txt");
		Scanner scan = new Scanner(inputFile);
		
		while(scan.hasNextDouble())
		{
			n = scan.nextDouble();
			m += n;
			i++;
		}
		n = m / i;
		System.out.println("Average is "+ n);
	}

}
