//Michael Gameng
//A20303150
//CS115-02

class Employee
{
	//declares the variables needed
	private String firstName;
	private String lastName;
	private int yearsInCompany;
	private double salary;
	private char status;
	private String section;
	private String divisionTitle;
	private int numberOfEmployeesInDivision;

	public static void main(String [] args)
	{
		//outputs the information
		Employee emp1 = new Employee();
		System.out.println("Name is " + emp1.GetFirstName() + " " + emp1.GetLastName());
		System.out.println("Salary is " + emp1.GetSalary());
		System.out.println("Years in company is " + emp1.GetYearsInCompany());
		System.out.println("Status is " + emp1.GetStatus());
		System.out.println("Section is " + emp1.GetSection());
		System.out.println("Division Title is " + emp1.GetDivisionTitle());
		System.out.println("Number of employees in division is " + emp1.GetNumberOfEmployeesInDivision());

		emp1.toString(firstName + lastName + yearsInCompany + salary + status + section + divisionTitle + numberOfEmployeesInDivision);

		if(emp1.equals(firstName + lastName + yearsInCompany + salary + status + section + divisionTitle + numberOfEmployeesInDivision))
			return(true);
		else
			return(false);
	}

	public Employee()
	{
		//initializes the variables with set values
		firstName = "NoFirstName";
		lastName = "NoLastName";
		yearsInCompany = 0;
		salary = 0.0;
		status = 'U';
		section = "Unknown";
		divisionTitle = "Unknown";
		numberOfEmployeesInDivision = 0;
		
	}

	public Employee(String startFirstName,
			String startLastName,
			int startYearsInCompany,
			double startSalary,
			char startStatus,
			String startSection,
			String startDivisionTitle,
			int startNumberOfEmployeesInDivision)
	{
		firstName = startFirstName;
		lastName = startLastName;
		yearsInCompany = startYearsInCompany;
		section = startSection;
		divisionTitle = startDivisionTitle;
		numberOfEmployeesInDivision = startNumberOfEmployeesInDivision;

		//validate startSalary parameter
		if(startSalary >= 0)
			salary = startSalary;
		else
		{
			System.err.println("Salary is negative.");
			System.err.println("Value set to 0.");
		}

		//validate startSection parameter
		switch(startSection)
		{
			case 'A': case 'a':
				System.out.println("active");
				break;
			case 'R': case 'r':
				System.out.println("retired);
				break;
			default:
				System.err.println("ERROR INVALID INPUT");
				break;
		}	
	}

	public String GetFirstName()
	{
		return firstName;
	}
	
	public String GetLastName()
	{
		return lastName;
	}
	
	public int GetYearsInCompany()
	{
		return yearsInCompany;
	}
	
	public double GetSalary()
	{
		return salary;
	}

	public char GetStatus()
	{
		return status;
	}

	public String GetSection()
	{
		return section;
	}

	public String GetDivisionTitle()
	{
		return divisionTitle;
	}

	public int GetNumberOfEmployeesInDivision()
	{
		return numberOfEmployeesInDivision;
	}
}