//Michael Gameng
//A20303150
//CS115-02

//import scanner, file input output
import java.io.*;
import java.util.Scanner;

public class MichaelGameng 
{
	//declare variables
	 String firstName;
	 String lastName;
	 int yearsInCompany;
	 double salary;
	 char status;
	 String section;
	 String divisionTitle;
	 int numberOfEmployeesInDivision;

	public static void main(String [] args) throws IOException
	{
		//declare scanner
		Scanner scan = new Scanner(System.in);
		FileOutputStream ofile = new FileOutputStream("empcs115output.txt", false);
		PrintWriter pw = new PrintWriter(ofile);
		
		File ifile = new File("empcs115.txt");
		Scanner scan2 = new Scanner(ifile);
		//declare scanner to read the file
		
		String data;
		//prompts the user if they want to call the menu or final stats
		System.out.print("Enter 'M' or 'm' to continue to the menu" +
						"\nEnter 'Q' or 'q' to display the number of times each menu option is entered" +
						"\nEnter your choice: ");
						
		String choice1 = scan.next();
		char choice = choice1.charAt(0);
		//constant for maximum calls
		final int MAXCALLS = 100;
		int [] stats = new int [MAXCALLS];

		for(i = 0; i < MAXCALLS; i++)
			{
				stats [i] = 
			}
		//prompts the user to choose what they want
		switch(choice)
		{
			case 'M': case 'm':
				data = menu(choice);
				System.out.println("menu() returned: " + data);
				break;
			case 'Q': case 'q':
				data = finalStats();
				break;
			default:
				System.out.print("Try again: ");
				break;
		}
	return;
	}
	
	public static String menu(char choice) throws IOException
		{
		int l, e, d, s, r, q, o;
		l = 1; e = 1; d = 1; s = 1; r = 1; q = 1; o = 1;
		//prompts the user what information they want
		Scanner scan = new Scanner(System.in);
		System.out.println("Press 'L' or 'l' for all of the employee data" +
						"\nPress 'E' or 'e' for a particular employee" +
						"\nPress 'D' or 'd' for division information" +
						"\nPress 'S' or 's' for salary information" +
						"\nPress 'R' or 'r' for retirement information" +
						"\nPress 'Q' or 'q' to quit" +
						"\nEnter your choice: ");
		String input = scan.next();
		choice = input.charAt(0);
		File ifile = new File("empcs115.txt");
		Scanner scan2 = new Scanner(ifile);
		String data;
		switch(choice)
		{
			case 'L': case 'l':
				data = listAll();
				l++;
				break;
			case 'E': case 'e':
				e++;
				System.out.print("Employee's first and last name: ");
				String particularEmployee = scan.next();
				String employee = particularEmployee.toLowerCase();
				while(scan.hasNext())
					{
						String input1 = scan.next();
						String input2 = input1.toLowerCase();
						pw.println(input1);
						
						if(employee.equals(input1))
							System.out.println(input1);
					}
				break;
			case 'D': case 'd':
				d++;
				System.out.println("Division: ");
				break;
			case 'S': case 's':
				s++;
				data = salaryReport(salary);
				break;
			case 'R': case 'r':
				r++;
				data = retirementReport();
				break;
			case 'Q': case 'q':
				q++;
				break;
			case 'O': case 'o':
				o++;
				data = sortList();
				break;
			default:
				System.out.print("Try again: ");
				break;
		}
		System.out.println("In menu()");
		return(input);
		}
		
	void static listAll(firstName, lastName, yearsInCompany, salary, status, section, divisionTitle, numberOfEmployeesInDivision)
		{
			System.out.println("Displaying all employee information: ");
				while(scan2.hasNext())	
					System.out.println(scan2.next());
			return();
		}
	
	public void employeeReport()
		{
			System.out.print("Employee's last name: ");
			String particularEmployee = scan.next();
			String employee = particularEmployee.toLowerCase();
			System.out.println("No employee found");
			return();
		}

	public divisionReport()
		{
			System.out.print("Enter division or type 'all': ");
			String divisionOutput = scan.next();
			String divisionOutput1 = divisionOutput.toLowerCase();
			switch(divisionOutput1)
			{
				case "all":
					System.out.println();
					break;
				case "occupational":
					System.out.println();
					break;
				case "management":
					System.out.println();
					break;
				case "temporary":
					System.out.println();
					break;
				default:
					break;
			}
			if(divisionOutput.equals(all))
				System.out.println();
			
		}

	public salaryReport(int salary)
		{
			System.out.print("Employee's last name: ");
			String particularEmployee = scan.next();
			String employee = particularEmployee.toLowerCase();
			System.out.println("No employee found");
			return(salary);
		}

	public retirementReport()
		{
			double pension = (yearsInCompany) * (0.2 * salary);
			return(pension);
		}
	
	public finalStats()
		{
			System.out.println("L: + l "+
					"\nE: + e "+
					"\nD: + d "+
					"\nR: + r "+
					"\nS: + s "+
					"\nQ: + q "+
					"\nO: + o ");
			return();
		}

	public sortList()
		{
		}
}
