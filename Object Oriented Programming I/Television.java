public class Television
{

	private String brand;
	private double price;
	
	public Television()
	{
	brand = "unknown";
	}
	public Television(String startBrand, double startPrice)
	{
	brand = startBrand;
	setPrice(startPrice);
	}
	public String getBrand()
	{
	return brand;
	}
	public double getPrice()
	{
	return price;
	}
	public void setBrand(String newBrand)
	{
	brand = newBrand;
	}
	public void setPrice(double newPrice)
	{
	price = newPrice;
	}
	
}