import java.util.Scanner;

public class lab4Number38

{
	public static void main(String [] args)
	{
		Scanner scan = new Scanner(System.in);
		
		boolean b1 = true;
		boolean b2 = false;
		
		System.out.print("Enter first integer: ");
		int a1 = scan.nextInt();
		System.out.println("First integer: " + a1);
		
		System.out.print("Enter second integer: ");
		int a2 = scan.nextInt();
		System.out.println("Second integer: " + a2);
		
		if (b2)
			System.out.println("b2 is true");
		else if (a1 <= 10 || a2 > 50)
		{
			System.out.print("a1 <= 10 or ");
			System.out.println("a2 >50");
		}
		else
			System.out.println("none of the above");
	}
}
			