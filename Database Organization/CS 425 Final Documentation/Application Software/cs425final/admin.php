<?php session_start();
include 'home.php';?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="style.css">
        <title>Admin</title>
    </head>
    <body
        <br>
        <form name="table" action="" method ="POST">
        <select name="tableName">
            <option>Admin</option>
            <option>Course</option>
            <option>CourseTA</option>
            <option>Enrollment1</option>
            <option>EnrollmentIG</option>
            <option>ForumComments</option>
            <option>ForumTitle</option>
            <option>InterestGroup</option>
            <option>LogIn</option>
            <option>Moderator</option>
            <option>Privacy</option>
            <option>Professor</option>
            <option>Student</option>
            <option>TA</option>
            <option>Users</option>
        </select>
            <input type="submit" value="SHOW" name="SHOW" />
        </form>
        <br>
        <form action="assign.php" method="GET">
            <input type="submit" value="ASSIGN TA" name="assign" />
            <input type="submit" value="ASSIGN MOD" name="assign" />
        </form>
        
        
        
        <?php
            if($_SERVER['REQUEST_METHOD'] == 'POST'){
              $selectAll = DB::getInstance()->selectAll($_POST['tableName']);
              $numfields = mysqli_num_fields($selectAll);
              $_SESSION['tableName'] = $_POST['tableName'];
              echo "<strong>".$_SESSION['tableName']."</strong><br><br>";
              echo "<table>\n<tr>";
              while ($property = mysqli_fetch_field($selectAll)) {
                  echo '<th>'.$property->name.'</th>';
              }
              echo "</tr>\n";
              $data = array();
              while ($row = mysqli_fetch_row($selectAll)) // Data
              { 
                $i = $row[0];
                echo '<tr><td>'.implode($row,'</td><td>')."</td><td>".
                    "<a href='update.php?id=$i'>Update</a>"."</td><td>"."<a href='delete.php'>Delete</a>"."</td></tr>\n"; 
                
              }
              $_SESSION['data'] = $data;
              echo "<tr><td>"."<a href='insert.php'>Insert</a>"."</td></tr>";
              
              echo "</table>\n";
            }

        ?>
    </body>
</html>
