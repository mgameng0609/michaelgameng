<?php session_start(); ?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="style.css">
        <title><?php echo $_GET['name']; ?></title>
    </head>
    <body>
        <h1><?php include "home.php"; ?> </h1>
        <br>
        <table border="1">
            <thead>
                <tr>
                    <th><?php echo $_GET['name']; ?></th>
                </tr>
            </thead>
            <tbody>
        <?php
        $_SESSION['title'] = $_GET['name'];
        $discussion = DB::getInstance()->getComments($_GET['name'], $_SESSION['ig_name']);
        if(mysqli_num_rows($discussion) < 1) {
          echo "<tr><td>";
          echo 'No posts';
          echo "</td></tr>";
        }
        while($row = mysqli_fetch_assoc($discussion)){
          echo "<tr><td>";
          echo "<strong>".$row['firstName']." ".$row['lastName']."</strong>"." - ".$row['comments']."<br";
          echo "</td></tr>";
        }
        
        ?>
            </tbody>
        </table>
        <a href="reply.php">Reply</a>
    </body>
</html>
