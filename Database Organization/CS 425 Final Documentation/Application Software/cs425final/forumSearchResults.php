<?php session_start();
include 'home.php'; ?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="style.css">
        <title></title>
    </head>
    <body>
        <?php
        $titleIg = explode('?ig=', $_GET['name']);
        ?>
        <table border="1">
            <thead>
                <tr>
                    <th><?php echo $titleIg[1]; ?></th>
                </tr>
            </thead>
            <tbody>
        <?php
        $_SESSION['title'] = $titleIg[1];
        $_SESSION['ig_name'] = $titleIg[0];
        $discussion = DB::getInstance()->getComments($_SESSION['title'], $_SESSION['ig_name']);
        if(mysqli_num_rows($discussion) < 1) {
          echo "<tr><td>";
          echo 'No posts';
          echo "</td></tr>";
        }
        while($row = mysqli_fetch_assoc($discussion)){
          echo "<tr><td>";
          echo "<strong>".$row['firstName']." ".$row['lastName']."</strong>"." - ".$row['comments']."<br";
          echo "</td></tr>";
        }
        
        ?>
            </tbody>
        </table>
        <a href="reply.php">Reply</a>
    </body>
</html>
