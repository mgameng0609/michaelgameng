<?php session_start(); ?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="style.css">
        <title><?php echo $_GET['name']; ?></title>
    </head>
    <body>
        <h1><?php include "home.php"; ?> </h1>
        <br>
        <form action="interestGroup.php" method="POST">
            <select name="filter">
                <option>Most popular</option>
                <option>Your most viewed</option>
            </select>
            <input type="submit" value="Filter" name="filter" />
        </form>
        <br>
        <form action="forumSearch.php" method="POST">
            <input type="text" name="forumSearch" value="" />
            <input type="submit" value="Search" name="Search" />
        </form>
        <br>
        <table border="1">
            <thead>
                <tr>
                    <th><?php echo $_GET['name']." Discussion Posts"; ?></th>
                </tr>
            </thead>
            <tbody>
        <?php
        require_once "Database.php";
           if($_SERVER['REQUEST_METHOD'] == "POST"){
             
             if($_POST['filterOption'] == 'Most popular'){
               $server = 'mostPopular';
             } 
             if($_POST['filterOption'] == 'Your most viewed'){
             $server = 'mostViewed';
             }
           } else {
             $server = 'norm';
           }
//           echo $server;
        $_SESSION['interestGroup'] = $_GET['name'];
        $getForumTitle = DB::getInstance()->getForumTitle($_SESSION['interestGroup']);
        $_SESSION['ig_name'] = $_GET['name'];
        while($row = mysqli_fetch_assoc($getForumTitle)){
            ?><tr><td>
            <?php
            echo DB::getInstance()->linkToTitle($row['title']);
            ?>
                    </td></tr>
                <?php
        }
       
        ?>
            
                   
            </tbody>
        </table>
        <a href="createPost.php">New Post</a>
        <?php // include 'createPost.php';
//        echo $_SESSION['title'];?>
    </body>
</html>
