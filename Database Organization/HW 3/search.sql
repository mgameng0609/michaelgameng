CREATE DEFINER=`root`@`localhost` PROCEDURE `search`(
IN student VARCHAR(50))
BEGIN
	DECLARE done INT DEFAULT FALSE;
	DECLARE v_firstName			VARCHAR(30);
    DECLARE v_lastName			VARCHAR(30);
    DECLARE v_email				VARCHAR(100);
    DECLARE v_degreeType		VARCHAR(20);
    DECLARE v_degreeStatus		VARCHAR(20);
    DECLARE v_semester			VARCHAR(20);
    DECLARE v_year				INT;
    DECLARE v_gpa				DECIMAL(3,2);
    DECLARE v_ig				VARCHAR(320);
    DECLARE v_ig1				VARCHAR(50);
    DECLARE v_igID				INT;
    DECLARE v_gpaPrivacy		BOOL;
    DECLARE v_igPrivacy			BOOL;
    DECLARE v_ig2				VARCHAR(320);
    DECLARE cur1 CURSOR FOR
		SELECT firstName, lastName, email, degreeType, degreeStatus, semester, year, s.gpa, p.gpa, interestGroup
        FROM Student s
        INNER JOIN Users u
        ON s.userId = u.id
        INNER JOIN Privacy p
        ON p.id = s.id
        WHERE firstName = SUBSTRING_INDEX(student, ' ', 1)
        AND lastName = SUBSTRING_INDEX(student, ' ', -1);
	DECLARE cur2 CURSOR FOR
		SELECT e.id, interestGroup
        FROM EnrollmentIG e
        INNER JOIN Users u
        ON u.id = e.id
		WHERE firstName = SUBSTRING_INDEX(student, ' ', 1)
        AND lastName = SUBSTRING_INDEX(student,' ',-1);
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    SET v_ig = ' ';
    
    OPEN cur1;
 
    search: LOOP
		FETCH cur1 INTO v_firstName, v_lastName, v_email, v_degreeType, v_degreeStatus, v_semester, v_year, v_gpa, v_gpaPrivacy, v_igPrivacy;
        IF done
        THEN LEAVE search;
        END IF;
        /* in privacy, 0 means public */
			IF v_gpaPrivacy = 0 THEN 
				IF v_igPrivacy = 0 THEN
                OPEN cur2;
                
                ig: LOOP
					FETCH cur2 into v_igID,v_ig1;
                    IF done
					THEN LEAVE ig;
					END IF;
                    
                    /* concatenates all interest group of the student */
                    SET v_ig = CONCAT(v_ig,' ', v_ig1);
                    
                END LOOP;
					SELECT v_firstName, v_lastName, v_email, v_degreeType, v_degreeStatus, v_semester, v_year, v_gpa,v_ig;
				CLOSE cur2;
                ELSE
		
				SELECT v_firstName, v_lastName, v_email, v_degreeType, v_degreeStatus, v_semester, v_year, v_gpa;
                END IF;
			ELSE
				IF v_igPrivacy = 0 THEN 
				OPEN cur2;
                
                ig: LOOP
					FETCH cur2 into v_igID,v_ig1;
                    IF done
					THEN LEAVE ig;
					END IF;
                    
                    /* concatenates all interest group of the student */
					SET v_ig = CONCAT(v_ig,' ', v_ig1);
                    
                END LOOP;
					SELECT v_firstName, v_lastName, v_email, v_degreeType, v_degreeStatus, v_semester, v_year, v_ig;
				CLOSE cur2;
                ELSE
				SELECT v_firstName, v_lastName, v_email, v_degreeType, v_degreeStatus, v_semester, v_year;
                END IF;
			END IF;
		
	END LOOP;
    CLOSE cur1;
 
	
	
END