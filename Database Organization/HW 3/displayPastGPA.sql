CREATE DEFINER=`root`@`localhost` PROCEDURE `displayPastGPA`(
in  v_lastname varchar(50))
BEGIN
if v_lastname in (select lastname from users)
then 
select
c.avggpa as Average_Gpa, 
c.coursetype as Course_Type,
c.coursenumber as CourseNumber,
u.lastname as Last_Name
from course c 
inner join professor p on p.id = c.profid 
inner join users u on u.id = p.userId
where lastname = v_lastname and u.job = 'PROFESSOR';
else
signal sqlstate '46000' set message_text = 'Error: The name of the professor does not exist.';
end if;
END