CREATE DEFINER=`root`@`localhost` PROCEDURE `popular`()
BEGIN
	SELECT COUNT(*) as numPosts, title
    FROM ForumTitle t
    INNER JOIN ForumComments c
    ON t.id = c.titleId
    GROUP BY c.titleId
    ORDER BY numPosts DESC LIMIT 3;
END