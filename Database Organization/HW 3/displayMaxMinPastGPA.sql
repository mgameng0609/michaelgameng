CREATE DEFINER=`root`@`localhost` PROCEDURE `displayMaxMinPastGPA`(
in  v_lastname varchar(50))
BEGIN
if v_lastname in (select lastname from users)
then 
select
MAX(c.avggpa) as Max_Average_Gpa,
MIN(c.avggpa) as Min_Average_Gpa
from course c 
inner join professor p on p.id = c.profid 
inner join users u on u.id = p.userId
where lastname = v_lastname and u.job = 'PROFESSOR';
else
signal sqlstate '46000' set message_text = 'Error: The name of the professor does not exist.';
end if;
END