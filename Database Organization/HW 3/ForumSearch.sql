CREATE DEFINER=`root`@`localhost` PROCEDURE `ForumSearch`(
search varchar(320))
BEGIN
Select interestGroup, title, comments, c.userId FROM ForumComments c
INNER JOIN ForumTitle t
ON t.id = c.titleId
Where c.comments Like search OR 
t.title Like search;
END