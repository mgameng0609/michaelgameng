CREATE DEFINER=`root`@`localhost` PROCEDURE `changePrivacy`(
IN userId1			VARCHAR(20),
IN gpaPrivacy		INT,
IN igPrivacy		INT)
BEGIN
    UPDATE Privacy
	SET gpa = gpaPrivacy,
		interestGroup = igPrivacy
	WHERE id = userId1;
	COMMIT;
END