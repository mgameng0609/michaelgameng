CREATE DEFINER=`root`@`localhost` PROCEDURE `recent`(
in ig varchar(50))
BEGIN
if ig in (select interestgroup from forumtitle)
then
select 
t.title as Forum_Title, 
c.comments as Comments, 
c.stamp as Time_Stamp
from forumtitle t
inner join forumcomments c
on c.titleid = t.id
where ig = t.interestgroup 
order by Time_Stamp desc limit 5;
else
signal sqlstate '45000' set message_text = 'Error: A discussion/comment for this title does not exist.';
end if;
END