/*
 * mm-naive.c - The fastest, least memory-efficient malloc package.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <unistd.h>

#include "mm.h"
#include "memlib.h"

/* double word (8) alignment */
#define ALIGNMENT 8
/* rounds up to the nearest multiple of ALIGNMENT */
#define ALIGN(size) (((size) + (ALIGNMENT-1)) & ~0x7)
#define SIZE_T_SIZE (ALIGN(sizeof(size_t)))
/* number of free lists */
#define FREE_LISTS  10
/* index of explicit free list */
#define EXPL_IDX   9
/* used bit, inuse and size */
#define BLOCK_USED 01
#define BLOCK_INUSE(p) (p->size & BLOCK_USED)
#define BLOCK_SIZE(p) (p->size & ~BLOCK_USED)
/* pointer offest & data */
#define BLOCK_PTR_OFFSET(p, offset) ((block*)((char*)p + offset))
#define BLOCK_PTR_DATA(p) ((void*)((char*)p + SIZE_T_SIZE))
/* does p live in the heap? */
#define IN_HEAP(p) ((void*)p >= mem_heap_lo() && (void*)p <= mem_heap_hi())

typedef struct block block;
struct block {
    size_t size;
    block* next;
};

/* sizes in seg list */
size_t lists_sizes[FREE_LISTS] =
{ 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 0 };

/* pointers to beginning of each list */
block *free_lists[FREE_LISTS];

/* returns the index of the smallest list that's able to hold size */
int fit_list(size_t size) {
    int fit = 0;
    long size_in = 16;
    
    while (size_in <= 4096) {
        if ((long)size <= size_in) return fit;
        fit++;
        size_in = size_in * 2;
    }
    return fit;
}

/* puts a block on the right size free list */
void put_free_block(block *b) {
    int i = fit_list(b->size);
    block *prev = free_lists[i];
    if (i != EXPL_IDX || b < prev || !prev) {
        b->next = free_lists[i];
        free_lists[i] = b;
        return;
    }
    while (prev->next && b > prev->next) {
        prev = prev->next;
    }
    b->next = prev->next;
    prev->next = b;
}

/* removes a block from its free list */
void mark_used(block *b) {
    int i = fit_list(b->size);
    block *j = free_lists[i];
    
    if (j == b) {
        free_lists[i] = j->next;
        return;
    }
    
    while (j && j->next != b) {
        j = j->next;
    }
    j->next = b->next;
}

/* splits block at offest and adds it to free list, returns b with new size */
block* split(block* b, size_t offset) {
    block* split = BLOCK_PTR_OFFSET(b, offset);
    
    if ((b->size - offset) <= lists_sizes[(EXPL_IDX - 1)]) return b;
    
    split->size = b->size - offset;
    split->next = b->next;
    b->size = offset;
    b->next = split;
    
    return b;
}

/* return a block able to fit size, split block if size < b->size */
block* find_fit(size_t size) {
    block *b = free_lists[EXPL_IDX];
    
    while (b != NULL) {
        /* coalese to right */
        while (b->size < size && b->next == BLOCK_PTR_OFFSET(b, b->size)) {
            b->size += b->next->size;
            b->next = b->next->next;
        }
        
        if (b->size > size) return split(b, size);
        else if (b->size == size) return b;
        
        b = b->next;
    }
    
    return 0; //no block of appropriate size found
}

/* makes/returns a free block able to fit size, doesn't add it to the free
 * list */
block* create_free_block(size_t size) {
    int i = fit_list(size);
    size_t block_size = lists_sizes[(i)];
    block *b;
    
    if (!block_size) block_size = size;
    b = mem_sbrk(block_size);
    
    if ((long)b == -1) return NULL;
    
    b->size = block_size & ~BLOCK_USED;
    b->next = NULL;
    
    return b;
}

/* initialize the heap */
int mm_init(void) {
    int i;
    for (i = 0; i < FREE_LISTS; i++) {
        free_lists[i] = NULL;
    }
    return 0;
}

/* allocates a new block of size, allocating more memory if needed */
void *mm_malloc(size_t size) {
    size_t newsize = ALIGN(size + SIZE_T_SIZE);
    int i = fit_list(newsize);
    block *b = free_lists[i];
    
    if (i == EXPL_IDX) b = find_fit(newsize);
    if (!b) b = create_free_block(newsize);
    else mark_used(b);
    b->size |= BLOCK_USED;
    
    return BLOCK_PTR_DATA(b);
}

/* frees a block and adds it to appropriate free list */
void mm_free(void *p) {
    block *b = BLOCK_PTR_OFFSET(p, -SIZE_T_SIZE);
    b->size &= ~BLOCK_USED;
    put_free_block(b);
}

/* joins block with right-side adjacent block */
void realloc_join(block *b) {
    while (b->next == BLOCK_PTR_OFFSET(b, b->size)) {
        b->size += b->next->size;
        b->next = b->next->next;
    }
}

/* attempts to join with free neighbors or mm_malloc's more memory */
void *mm_realloc(void *p, size_t size) {
    size_t newsize = ALIGN(size + SIZE_T_SIZE);
    int i = fit_list(newsize);
    block *b = BLOCK_PTR_OFFSET(p, -SIZE_T_SIZE);
    block *other;
    void *new_p;
    size_t copy_size;
    
    if (BLOCK_SIZE(b) >= newsize) return p;
    
    if (i == EXPL_IDX) {
        other = BLOCK_PTR_OFFSET(b, BLOCK_SIZE(b));
        if ((void*)other < mem_heap_hi() && !BLOCK_INUSE(other)) {
            realloc_join(other);
            if ((BLOCK_SIZE(b) + other->size) > newsize)
                other = split(other, newsize - BLOCK_SIZE(b));
            if ((BLOCK_SIZE(b) + other->size) == newsize) {
                b->size += other->size;
                mark_used(other);
                
                return BLOCK_PTR_DATA(b);
            }
        }
        if ((void*)BLOCK_PTR_OFFSET(b, BLOCK_SIZE(b)) >= mem_heap_hi()) {
            new_p = mem_sbrk(newsize - BLOCK_SIZE(b));
            
            if ((long)b == -1) return NULL;
            
            b->size += newsize - BLOCK_SIZE(b);
            
            return BLOCK_PTR_DATA(b);
        }
    }
    
    new_p = mm_malloc(size);
    
    if (!new_p) return NULL;
    
    other = BLOCK_PTR_OFFSET(new_p, -SIZE_T_SIZE);
    copy_size = BLOCK_SIZE(other) - SIZE_T_SIZE;
    if (size < copy_size) copy_size = size;
    memcpy(BLOCK_PTR_DATA(other), p, copy_size);
    mm_free(p);
    
    return new_p;	
}

/* 1 if b is in free list, 0 otherwise */
int found_free(block* b) {
    int i = fit_list(b->size);
    block *j = free_lists[i];
    while (j) {
        if (j == b) return 1;
        j = j->next;
    }
    return 0;
}
